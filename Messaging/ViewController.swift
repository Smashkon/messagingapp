//
//  ViewController.swift
//  Messaging
//
//  Created by Ashkon Roschanzamir on 18/01/2016.
//  Copyright © 2016 Ashkon Roschanzamir. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet var inputField: UITextField!
    @IBOutlet var outputTextView: UITextView!
    private var message: Message!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        let tapGestureRecogniser = UITapGestureRecognizer(target: self, action: Selector("hideKeyboard"))
        self.view.addGestureRecognizer(tapGestureRecogniser)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func hideKeyboard() {
        self.inputField.resignFirstResponder()
    }
    
    @IBAction func submitMessage(sender: UIButton?) {
        self.hideKeyboard()
        if let message = self.inputField.text {
            self.message = Message(contents: message)
            self.message.analyseMessage({ () -> () in
                // Output the text
                self.outputTextView.text = self.message.createMessageJSON()
            })
            
        }
        
    }


}

