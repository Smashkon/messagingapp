//
//  Message.swift
//  Messaging
//
//  Created by Ashkon Roschanzamir on 18/01/2016.
//  Copyright © 2016 Ashkon Roschanzamir. All rights reserved.
//

import UIKit

class Message: NSObject {
    
    var contents: String!
    private lazy var mentions = [String]()
    private lazy var links = [[String : String]]()
    private lazy var emoticons = [String]()
    
    init(contents: String) {
        super.init()
        self.contents = contents
        
    }
    
    func analyseMessage(success: () -> ()) {
        // Break up the string into mentions, links and emoticons
        
        let wordArray = self.contents.componentsSeparatedByCharactersInSet(NSCharacterSet.whitespaceCharacterSet()).filter { !$0.isEmpty }
        let queue: dispatch_queue_t = dispatch_queue_create("ConcurrentQueue", DISPATCH_QUEUE_CONCURRENT)
        let group: dispatch_group_t = dispatch_group_create()
        
        for word in wordArray {
            // Find the mentions
            dispatch_group_async(group, queue, { () -> Void in
                print("dispatched word check")
                if self.checkMention(word) == true {
                    // If it starts with @, we remove the @ and save it as a mention
                    let mention = String(word.characters.dropFirst())
                    self.mentions.append(mention)
                }
                
                
                // Find the emoticons
                if self.checkEmoticon(word) == true {
                    let possibleEmoticon = String(word.characters.dropFirst().dropLast())
                    self.emoticons.append(possibleEmoticon)
                }
                
                // Find the URLs
                if self.checkURL(word) == true {
                    var newLink = word
                    if word.hasPrefix("http") == false && word.containsString(".com") {
                        newLink = "http://" + word
                    }
                    self.getTitleOfUrl(newLink, success: { (title: String?) -> () in
                        if title != nil {
                            self.links.append(["url" : newLink, "title" : title!])
                        }
                        
                        }, fail: { (error: NSError?) -> () in
                            if error != nil {
                                print("Error retrieving title of link: \(error!.localizedDescription)")
                            }
                            self.links.append(["url" : newLink, "title" : ""])
                    })
                }
                
            })
        }
        dispatch_group_notify(group, queue, { () -> Void in
            print("dispatch notify")
            dispatch_sync(dispatch_get_main_queue(), { () -> Void in
                success()
            })
        })

    }
    
    func createMessageJSON() -> String {
        // Create the message JSON string from the analysed message
        var jsonArray = [[String : AnyObject]]()
        if self.mentions.count > 0 {
            let dict = ["mentions" : self.mentions]
            jsonArray.append(dict)
        }
        if self.emoticons.count > 0 {
            let dict = ["emoticons" : self.emoticons]
            jsonArray.append(dict)
        }
        if self.links.count > 0 {
            let dict = ["links" : self.links]
            jsonArray.append(dict)
        }
        
        do {
            let jsonData = try NSJSONSerialization.dataWithJSONObject(jsonArray, options: NSJSONWritingOptions(rawValue: 1))
            let jsonString = String(data: jsonData, encoding: NSASCIIStringEncoding)
            print(jsonString!)
            return jsonString!
        }
        catch let error as NSError {
            print("Error creating JSON: \(error.localizedDescription)")
        }
        
        return ""
    }
    
    
    
    //MARK: - Helpers
    
    private func checkMention(word: String) -> Bool {
        // Check if word is a mention
        if word.characters.first == "@" {
            return true
        }
        return false
    }
    
    private func checkEmoticon(word: String) -> Bool {
        // Check if word is an emoticon
        if word.characters.first == "(" && word.characters.last == ")" {
            let possibleEmoticon = String(word.characters.dropFirst().dropLast())
            let alphaNumericCharSet = NSCharacterSet.alphanumericCharacterSet()
            if possibleEmoticon.stringByTrimmingCharactersInSet(alphaNumericCharSet) == "" && possibleEmoticon.characters.count <= 15 {
                return true
            }
        }
        return false
    }
    
    private func checkURL(var word: String) -> Bool {
        // Check if the word is a URL
        if word.hasPrefix("http") == false && word.containsString(".com") {
            word = "http://" + word
        }
        if let url = NSURL(string: word) {
            if url.host != nil {
                return true
            }
        }
        return false
    }
    
    private func getTitleOfUrl(url: String, success: (response: String?) -> (), fail: (error : NSError?) -> ()) {
        // Loads the URL asynchronously, parses HTML to get the title
        if let url = NSURL(string: url) {
            let taskResult = NSURLSession.sharedSession().synchronousDataTaskWithURL(url)
            if taskResult.2 == nil {
                // Add the link if we have the title
                if let responseData = taskResult.0 {
                    let site = TFHpple(data: responseData, isXML: false)
                    let elements = site.searchWithXPathQuery("/html/head/title")
                    if let element = elements[0] as? TFHppleElement {
                        success(response: element.text())
                    }
                }
            }
            else {
                fail(error: taskResult.2)
            }
            
        }
    }
    
    
}
